#!/bin/sh
# screenshot.sh: use maim+slop to take a screenshot
TIMESTAMP=$(date +"%m-%d-%Y-%T");
notify-send -t 0 "image captured" "$(maim -m 10 -g $(slop) ~/Pictures/$TIMESTAMP.jpg)"
