(require 'package)
(add-to-list 'package-archives
 '("melpa" . "https://melpa.org/packages/") t)
(add-to-list 'package-archives
 '("org" . "https://orgmode.org/elpa/") t)
(package-initialize)
(custom-set-variables
 '(custom-enabled-themes '(nyan))
 '(custom-safe-themes
   '("f3d88904f7c4a25cd6d0fad6cf1f5da9045a83a056f97540127fc7f8c26f8caf" default))
 '(custom-theme-directory "~/.config/emacs/themes")
 '(package-selected-packages '(ampc multi-vterm sudo-edit vterm org))
 '(tab-bar-mode t)
 '(tab-bar-show t)
 '(use-file-dialog nil))
(custom-set-faces
 '(default ((t (:family "lucy tewi" :foundry "lucy" :slant normal :weight normal :height 83 :width normal))))
 '(fringe ((t nil)))
 '(tab-bar-tab ((t (:inherit tab-bar :box (:line-width 1 :style released-button)))))
 '(tab-bar-tab-inactive ((t (:inherit tab-bar-tab :background "grey75")))))
